class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  require 'rubygems'
  require 'mvg/live'
  require 'json'
  def hello
    json_String = MVG::Live.fetch 'Sendlinger Tor'
        # JSON.parse(json_String, object_class: OpenStruct)
    result  = ""
    json_String.each do |p|
      result += p[:line]+"\t"+p[:destination]+"\t"+p[:minutes].to_s+"\n"
    end
    #result = raw ('<div>'+result+'<div>')
    render html: result
  end

  def html_test
    #render html: '<div>Hallo<br>wie gehts</div>'.html_safe
    render 'layouts/application'
  end

end

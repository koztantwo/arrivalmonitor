class BookController < ActionController::Base
  protect_from_forgery with: :exception
  require 'rubygems'
  require 'mvg/live'
  require 'json'


  @@stations = ['Stiglmaierplatz', 'Königsplatz']
  @@blacklist = ['Olympia - Einkaufszentrum', 'Moosach Bf.', 'Messestadt Ost','100', 'Borstei', 'Hauptbahnhof Nord', 'Hochschule München']

  helper_method :index
  def index
    results = Array.new
    @@stations.each do |station|
      begin
      results.push ((MVG::Live.fetch station))
      rescue
# ignored
        config.logger = Logger.new(STDOUT)
        logger.debug 'there was an error'
      end
    end
    results = results.flatten(1)
    results = sort(filter(results))
    @books = results
  end

  def filter (arrivals)
    arrivals.select do |arrival|
      begin
      (!@@blacklist.include? arrival[:destination].to_s) && (!@@blacklist.include? arrival[:line].to_s)
      rescue
        false
      end
    end
  end

  def sort(arrivals)
    arrivals.sort_by {|e| e[:minutes]}
  end
end